
var target = UIATarget.localTarget();

target.frontMostApp().mainWindow().tableViews()[0].cells()[1].dragInsideWithOptions({
    startOffset: {x:0.8, y:0.1}, 
    endOffset: {x:0.1, y:0.1}, 
    duration: 0.25
  });
target.frontMostApp().mainWindow().tableViews()[0].cells()[1].buttons()["Delete"].tap();

target.frontMostApp().mainWindow().tableViews()[0].cells()[0].dragInsideWithOptions({
    startOffset: {x:0.8, y:0.1}, 
    endOffset: {x:0.1, y:0.1}, 
    duration: 0.25
  });
target.frontMostApp().mainWindow().tableViews()[0].cells()[0].buttons()["Delete"].tap();