//
//  RMAppDelegate.h
//  RMAutomationBug
//
//  Created by Ryan Mathews on 3/12/14.
//  Copyright (c) 2014 Ryan Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
