//
//  RMBasicTableViewController.m
//  RMAutomationBug
//
//  Created by Ryan Mathews on 3/12/14.
//  Copyright (c) 2014 Ryan Mathews. All rights reserved.
//

#import "RMBasicTableViewController.h"

@interface RMBasicTableViewController ()
@property (nonatomic, strong) NSMutableArray *titles;
@end

@implementation RMBasicTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setAccessibilityLabel:@"Basic Table"];
    self.title = @"Basic Table";
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Reset"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(resetPushed:)];
    
    self.navigationItem.rightBarButtonItem = button;
    [self resetPushed:nil];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
}

- (void)resetPushed:(id)sender
{
    self.titles = [NSMutableArray arrayWithArray:@[@"First Cell", @"Second Cell", @"Third Cell"]];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.textLabel setText:[self.titles objectAtIndex:indexPath.row]];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.titles removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

@end
